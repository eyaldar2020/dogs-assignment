import React from "react";
import axios from "axios";
import StyledIndex from "./index.styles";

const Index = () => {
  const [breedsArray, setBreedsArray] = React.useState([]);
  const [displayImagesArray, setDisplayImagesArray] = React.useState([]);
  const [selectedImage, setSelectedImageIndex] = React.useState();

  const fetchBreeds = async () => {
    const response = await axios.get("https://dog.ceo/api/breeds/list/all");

    let arrayOfBreeds = [];
    for (let breed in response.data.message) {
      arrayOfBreeds.push(breed);
    }

    setBreedsArray(arrayOfBreeds);
  };

  React.useEffect(() => {
    fetchBreeds();
  }, []);

  const fetchByBreed = async (breed) => {
    const response = await axios.get(
      `https://dog.ceo/api/breed/${breed}/images`
    );

    setDisplayImagesArray(response.data.message);
    // eraseSelectedImage
    setSelectedImageIndex(null);
  };

  // fetch list of images by breed
  const handleSelect = (e) => fetchByBreed(e.target.value);

  const handleClick = (e) => setSelectedImageIndex(e.target.alt);

  return (
    <StyledIndex>
      <h1>dogs app</h1>
      <div className="container">
        <div className="title">
          <label htmlFor="selectBreed">breed: </label>
          <select id="selectBreed" onChange={handleSelect}>
            {/* get breed kinds from api */}
            {breedsArray.map((breed, i) => {
              return <option key={i}>{breed}</option>;
            })}
          </select>
        </div>
        <div className="img-container">
          {displayImagesArray.map((pic, i) => {
            return (
              <button type="button" onClick={handleClick} key={i}>
                <img
                  src={pic}
                  alt={i} // would have done in props - on another component...
                  className={selectedImage === i.toString() && "selected"}
                />
              </button>
            );
          })}
        </div>
      </div>
    </StyledIndex>
  );
};

export default Index;
