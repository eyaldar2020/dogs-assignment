import styled from "styled-components";

const StyledIndex = styled.div`
  background-color: red;

  * {
    box-sizing: border-box;
    padding: 0;
    margin: 0;
    border: 0;
  }

  .img-container {
    background-color: green;
    padding: 5vw;

    display: grid;
    column-gap: 10px;
    row-gap: 10px;
    grid-template-columns: repeat(auto-fill, minmax(160px, 1fr));
  }

  button {
    width: 160px;
    height: 160px;
  }

  img {
    width: 100%;
    height: 100%;
  }

  .selected {
    border: 10px yellow solid;
  }
`;

export default StyledIndex;
